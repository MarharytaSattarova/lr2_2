﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TRStPO_Sattarova_LR2_2
{
    public partial class Form1 : Form
    {
        public int MatrixSize = 3;

        public double[][] A = {new double[] {4, 7, 2}, new double[] {9, 1, 3}, new double[] {2, 8, 2}};
        public double[][] A1 = {new double[] {9, 1, 2}, new double[] {11, 8, 2}, new double[] {2, 6, 3}};
        public double[][] A2 = {new double[] {10, 2, 2}, new double[] {0, 4, 4}, new double[] {-4, 7, -2}};

        public double[] b;


        public double[][] C2;
        private double[][] Y3;

        public double[] b1 = {5, 6, 7};
        public double[][] B2 = {new double[] {8, 8, 1}, new double[] {2, 6, 4}, new double[] {1, 3, 5}};
        public double[] c1 = {8, 4, 2};

        public double[] d1;
        public double[] y1;
        public double[] y2;

        public Form1()
        {
            InitializeComponent();
        }

        private void PopulateDataGridViewsWithData()
        {
            dataGridView1.Rows.Add(MatrixSize);
            dataGridView2.Rows.Add(MatrixSize);
            dataGridView3.Rows.Add(MatrixSize);
            dataGridView4.Rows.Add(MatrixSize);
            dataGridView5.Rows.Add(MatrixSize);
            dataGridView6.Rows.Add(MatrixSize);

            for (var i = 0; i < MatrixSize; i++)
            {
                for (var j = 0; j < MatrixSize; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = A[i][j].ToString();
                    dataGridView2.Rows[i].Cells[j].Value = A1[i][j].ToString();
                    dataGridView3.Rows[i].Cells[j].Value = A2[i][j].ToString();
                    dataGridView4.Rows[i].Cells[j].Value = B2[i][j].ToString();
                }

                dataGridView5.Rows[i].Cells[0].Value = b1[i].ToString();
                dataGridView6.Rows[i].Cells[0].Value = c1[i].ToString();
            }
        }

        private double[] ParallelMultiplicationOfMatrixAndVector(double[][] matrix, double[] vector)
        {
            var result = new double[MatrixSize];
            Parallel.For(0, MatrixSize,
                i => { Parallel.For(0, MatrixSize, j => { result[i] += matrix[i][j] * vector[j]; }); });

            return result;
        }

        private void CalculateY1()
        {
            CalculateB();

            y1 = ParallelMultiplicationOfMatrixAndVector(A, b);

            richTextBox1.Text += "b:\n";
            richTextBox1.Text += GetStringInterpretationOfVector(b);
            richTextBox1.Text += "y1:\n";
            richTextBox1.Text += GetStringInterpretationOfVector(y1);
        }

        private void CalculateB()
        {
            b = new double[MatrixSize];

            Parallel.For(0, MatrixSize, i => { b[i] = i % 2 == 0 ? 1 / (Math.Pow(i, 2) + 2) : 1 / i; });
        }

        private void CalculateY2()
        {
            CalculateD1();

            y2 = ParallelMultiplicationOfMatrixAndVector(A1, d1);

            richTextBox1.Text += "d1:\n";
            richTextBox1.Text += GetStringInterpretationOfVector(d1);
            richTextBox1.Text += "y2:\n";
            richTextBox1.Text += GetStringInterpretationOfVector(y2);
        }

        private void CalculateD1()
        {
            d1 = new double[MatrixSize];

            Parallel.For(0, MatrixSize, i => { d1[i] = b1[i] + c1[i]; });
        }

        public void CalculateY3()
        {
            for (var i = 0; i < MatrixSize; i++) Y3[i] = new double[MatrixSize];

            var D = new double[MatrixSize, MatrixSize];

            Parallel.For(0, MatrixSize, i =>
            {
                C2[i] = new double[MatrixSize];
                Parallel.For(0, MatrixSize, j =>
                {
                    C2[i][j] = 1.0 / (i + 1 + 2 * j);
                    D[i, j] = B2[i][j] - C2[i][j];
                    Parallel.For(0, MatrixSize, k => { Y3[k][j] += A2[k][i] * D[i, j]; });
                });
            });

            richTextBox1.Text += "C2:\n";
            richTextBox1.Text += GetStringInterpretationOfMatrix(C2);
            richTextBox1.Text += "Y3:\n";
            richTextBox1.Text += GetStringInterpretationOfMatrix(Y3);
        }

        private string GetStringInterpretationOfMatrix(double[][] array)
        {
            var output = string.Empty;

            foreach (var row in array) output += GetStringInterpretationOfVector(row);

            return output;
        }

        private string GetStringInterpretationOfVector(double[] array)
        {
            var output = string.Empty;

            output = array.Aggregate(output, (current, element) => current + $"{element:F}\t");
            output += '\n';

            return output;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MatrixSize = int.Parse(textBox1.Text);

            C2 = new double[MatrixSize][];
            Y3 = new double[MatrixSize][];

            PopulateDataGridViewsWithData();

            CalculateY1();
            CalculateY2();
            CalculateY3();
        }
    }
}